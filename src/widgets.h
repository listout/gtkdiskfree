/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#ifndef GTKDISKFREE_WIDGET_H
#define GTKDISKFREE_WIDGET_H
#define HORIZONTAL 0
#define VERTICAL   1

#include <gtk/gtk.h>

/* Buttons */
GtkWidget *widget_button_color_add (GtkWidget *, GCallback, const gint, gdouble *, const gchar *);

/* Menus */
GtkWidget *widget_menu_add (GtkWidget *item);
GtkWidget *widget_menu_item_add (GtkWidget *menu, const gchar *title);

/* Frames */
GtkWidget *widget_frame_add (GtkWidget *box, const gchar *title);

/* Checks */
GtkWidget *widget_check_add (GtkWidget *box, gint *value, const gchar *title);

/* Spins */
GtkWidget *widget_spin_add_float (GtkWidget *box, gfloat *value,
                    gfloat max, gfloat min, gfloat step,
                    const gchar *title);
GtkWidget *widget_spin_add_int (GtkWidget *box, gint *value,
                gint max, gint min, gint step,
                const gchar *title);

/* Radios */
GtkWidget *widget_radio_add (GtkWidget *box, GtkWidget *from,
                    gint val, gint *value,
                    const gchar *title);

/* Entrys */
GtkWidget *widget_entry_add_chars (GtkWidget *box, gchar **value, const gchar *title);

void widget_entry_get_chars (GtkWidget *entry, gchar *text[]);
void widget_entry_get_list (GtkWidget *entry, GList **glist, const gchar *del);

/* All */
void widget_set_sensitivity_invert (GtkWidget *widget);
#endif // GTKDISKFREE_WIDGET_H
