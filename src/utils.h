/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#ifndef GTKDISKFREE_UTILS_H
#define GTKDISKFREE_UTILS_H
#define FILE_TYPE      3
#define DIRECTORY_TYPE 4
#define UNKNOW_TYPE    5
#define NOEXIST_TYPE   6
#define MAXLINE     1024

#define COMMENT_CHAR             "#"
#define X_CHAR                   "x"
#define DIR_CHAR                 "/"
#define LIST_CHAR                ";"
#define SPACE_CHAR               " "
#define DEFCFG_CHAR              "="

#include <glib.h>
#include "main.h"
#define ERROR(args) (printf("** gtkdiskfree error: "), printf args, printf(" **\n"))

/* File utils */
gint filutils_get_type (const gchar *);
gchar **filutils_get_lines (const gchar *);

/* String utils */
gint strutils_count_char (const gchar *, gchar);
gchar *strutils_get_arg (gchar **, const gchar *, const gchar *);
gchar *strutils_glist_concat(GList *list, const gchar *del);
GList *strutils_strdelim_to_glist(const gchar *arg, const gchar *del);

/* Commands */
gboolean open_cmdtube (const gchar *cmd, const gchar *mount_point, gchar **error);
#endif // GTKDISKFREE_UTILS_H
