/*
    GtkDiskFree shows free space on your mounted partitions.  Copyright
    (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
    USA */

#include "diskfree.h"
#include "configure.h"
#include "interface.h"
#include "pixmap.h"

#ifdef STDC_HEADERS
# include <stdlib.h>
#endif

#ifdef HAVE_MATH_H
# include <math.h>
#endif

#ifdef HAVE_MNTENT_H            /* 4.3BSD, SunOS, HP-UX, Dynix, Irix, Linux. */
# include <mntent.h>
#endif

#ifdef HAVE_SYS_VFSTAB_H
# include <sys/vfstab.h>
#endif

#if defined __SOLARIS__ || defined __LINUX__
# ifdef HAVE_SYS_STATVFS_H
#  include <sys/statvfs.h>
# endif
#else
# ifdef HAVE_SYS_STATFS_H
#  include <sys/statfs.h>
# endif
#endif

#ifdef HAVE_SYS_VFS_H
# include <sys/vfs.h>
#endif

gint gui_list_updated = FALSE;

void gui_list_update (order_t action, filesystem_t *fs, GtkTreeIter *riter);

filesystem_t * filesystem_new (gint val)
{
    filesystem_t *fs = g_malloc0(sizeof(filesystem_t));

    fs->mounted = val;

    return fs;
}

void filesystem_free (filesystem_t *fs)
{
    if (fs->pixbuf != NULL)
        g_object_unref(fs->pixbuf);
    if (fs->path != NULL)
        gtk_tree_path_free(fs->path);
    g_free(fs->fs_name);
    g_free(fs->fs_mntpt);
    g_free(fs->fs_options);
    g_free(fs->fs_type);
    g_free(fs->fs_size);
    g_free(fs->fs_used);
    g_free(fs->fs_free);
    g_free(fs->fs_pused);
    g_free(fs->fs_pfree);

    g_free(fs);

    return;
}

gint gui_list_iter_get_position (GtkTreeIter *iter)
{
    gint i;
    gchar *str;
    GtkTreePath *path;

    path = gtk_tree_model_get_path(list_treemodel, iter);
    str = gtk_tree_path_to_string(path);
    i = atoi(str);
    g_free(str);
    gtk_tree_path_free(path);

    return i;
}

gboolean gui_list_set_line_status (GtkTreePath *path, gint val)
{

    GtkTreeIter iter;

    if (gtk_tree_model_get_iter(list_treemodel, &iter, path)) {
        gtk_list_store_set(GTK_LIST_STORE(list_treemodel), &iter, STATUS_COLUMN, val, -1);

        return TRUE;
    }

    return FALSE;
}


gboolean gui_list_set_line_removable (GtkTreeModel *model, GtkTreePath *path,
                    GtkTreeIter *iter, gpointer data)
{
    gtk_list_store_set(GTK_LIST_STORE(model), iter, STATUS_COLUMN, FS_OLD, -1);

    return FALSE;
}

gboolean gui_list_check_cell_int (GtkTreeIter *iter, gint column, gint value)
{
    gint view;

    gtk_tree_model_get(list_treemodel, iter, column, &view, -1);
    if (view == value)
        return TRUE;

    return FALSE;
}

/* Just update. */
void gui_list_update (order_t action, filesystem_t *fs, GtkTreeIter *riter)
{
    static GdkPixbuf *mounted = NULL;
    static GdkPixbuf *unmounted = NULL;
    GtkTreeIter iter;

    if(mounted == NULL && unmounted == NULL) {
        mounted = pixmap_draw_mount(NULL, 12, options->color1);
        unmounted = pixmap_draw_mount(NULL, 12, options->color2);
    }

    gui_list_updated = TRUE;
    switch (action) {
        case ADD:
            gtk_list_store_append(GTK_LIST_STORE(list_treemodel), &iter);
            break;
        case INSERT:
            gtk_list_store_insert(GTK_LIST_STORE(list_treemodel), &iter,
                            gui_list_iter_get_position(riter));
            break;
        case UPDATE:
            gtk_tree_model_get_iter(list_treemodel, &iter, fs->path);
            break;
        case REMOVE:
            gtk_list_store_remove(GTK_LIST_STORE(list_treemodel), riter);
            return;
        default:
            return;
    }
    gtk_list_store_set(GTK_LIST_STORE(list_treemodel), &iter, FILESYSTEM_COLUMN, fs->fs_name,
                SIZE_COLUMN, fs->fs_size, USED_SPACE_COLUMN, fs->fs_used,
                FREE_SPACE_COLUMN, fs->fs_free, USED_PERCENT_COLUMN, fs->fs_pused,
                FREE_PERCENT_COLUMN, fs->fs_pfree, FILESYSTEM_TYPE_COLUMN, fs->fs_type,
                MOUNT_OPTIONS_COLUMN, fs->fs_options, MOUNT_POINT_COLUMN, fs->fs_mntpt,
                SIZEK_COLUMN, fs->size, USEDK_SPACE_COLUMN, fs->used,
                FREEK_SPACE_COLUMN, fs->free, USEDP_PERCENT_COLUMN, fs->pused,
                FREEP_PERCENT_COLUMN, fs->pfree, STATUS_COLUMN, fs->mounted, -1);

    if(fs->mounted == FS_MOUNTED)
        gtk_list_store_set(GTK_LIST_STORE(list_treemodel), &iter, MOUNT_COLUMN, mounted, -1);
    else if(fs->mounted == FS_UMOUNTED)
        gtk_list_store_set(GTK_LIST_STORE(list_treemodel), &iter, MOUNT_COLUMN, unmounted, -1);

    if (fs->pixbuf != NULL)
        gtk_list_store_set(GTK_LIST_STORE(list_treemodel), &iter, CAPACITY_COLUMN, fs->pixbuf, -1);

    return;
}

void gui_list_remove_old_lines (void)
{
    gboolean ret;
    GtkTreeIter iter;
    GtkTreePath *path = gtk_tree_path_new_first();

    ret = gtk_tree_model_get_iter(list_treemodel, &iter, path);

    while (ret) {
        if (gui_list_check_cell_int(&iter, STATUS_COLUMN, FS_OLD))
            gui_list_update(REMOVE, NULL, &iter);
        else
            gtk_tree_path_next(path);
        ret = gtk_tree_model_get_iter(list_treemodel, &iter, path);
    }

    gtk_tree_path_free(path);

    return;
}

gboolean gui_list_check_cell_chars (GtkTreeIter *iter, gint column, gchar *text)
{
    gint ret;
    gchar *view;

    gtk_tree_model_get(list_treemodel, iter, column, &view, -1);
    ret = strcmp(text, view);
    g_free(view);

    if (ret)
        return FALSE;
    else
        return TRUE;
}

gboolean gui_list_check_cell_ulong (GtkTreeIter *iter, gint column, gulong value)
{
    gulong view;

    gtk_tree_model_get(list_treemodel, iter, column, &view, -1);
    if (view == value)
        return TRUE;

    return FALSE;
}

GtkTreePath *gui_list_find_row (gchar *mntpt)
{
    GtkTreeIter iter;
    GtkTreePath *path = NULL;

    if (!gtk_tree_model_get_iter_first(list_treemodel, &iter))
        return path;

    do {
        if (gui_list_check_cell_chars(&iter, MOUNT_POINT_COLUMN, mntpt)) {
            path = gtk_tree_model_get_path(list_treemodel, &iter);
            break;
        }
    } while (gtk_tree_model_iter_next(list_treemodel, &iter));

    return path;
}

/* Line needs to be updated ? The number of the first cell which needs
 * to be updated is returned, else -1 is return. */
gint gui_list_row_needs_update (filesystem_t *fs)
{
    GtkTreeIter iter;

    if (gui_color_update || fs->path == NULL)
        return -1;

    gtk_tree_model_get_iter(list_treemodel, &iter, fs->path);

    if (!gui_list_check_cell_chars(&iter, FILESYSTEM_COLUMN, fs->fs_name))
        return FILESYSTEM_COLUMN;
    if (!gui_list_check_cell_chars(&iter, SIZE_COLUMN, fs->fs_size))
        return SIZE_COLUMN;
    if (!gui_list_check_cell_chars(&iter, USED_SPACE_COLUMN, fs->fs_used))
        return USED_SPACE_COLUMN;
    if (!gui_list_check_cell_chars(&iter, FREE_SPACE_COLUMN, fs->fs_free))
        return FREE_SPACE_COLUMN;
    if (!gui_list_check_cell_chars(&iter, MOUNT_OPTIONS_COLUMN, fs->fs_options))
        return MOUNT_OPTIONS_COLUMN;
    if (!gui_list_check_cell_chars(&iter, MOUNT_POINT_COLUMN, fs->fs_mntpt))
        return MOUNT_POINT_COLUMN;

    if (!gui_list_check_cell_ulong(&iter, SIZEK_COLUMN, fs->size))
        return SIZEK_COLUMN;
    if (!gui_list_check_cell_ulong(&iter, USEDK_SPACE_COLUMN, fs->used))
        return USEDK_SPACE_COLUMN;
    if (!gui_list_check_cell_ulong(&iter, FREEK_SPACE_COLUMN, fs->free))
        return FREEK_SPACE_COLUMN;
    if (!gui_list_check_cell_int(&iter, USEDP_PERCENT_COLUMN, fs->pused))
        return USEDP_PERCENT_COLUMN;
    if (!gui_list_check_cell_int(&iter, FREEP_PERCENT_COLUMN, fs->pfree))
        return FREEP_PERCENT_COLUMN;

    return 0;
}

/* Check if it needs to be displayed. According to options->blocks[1] and [2] */
void gui_list_pre_update (filesystem_t *fs)
{
    if (fs->path != NULL) {
        if (fs->need_update) {
            /* We need to know the new path of line if one line has been added. */
            gtk_tree_path_free(fs->path);
            fs->path = gui_list_find_row(fs->fs_mntpt);
            gui_list_update(UPDATE, fs, NULL);
        } else
            gui_list_set_line_status(fs->path, fs->mounted);
    } else
        gui_list_update(ADD, fs, NULL);

    return;
}

/* Return the percent. */
gint list_filesystem_percent (gulong all, gulong value)
{
    gfloat percent;

    percent = (float) value / all;
    percent = percent * 100;

    return (gint) percent;
}

/* Set values format. According to options->blocks */
gchar *list_filesystem_size (gulong value)
{
    gfloat test;
    gchar *size = NULL;

    if (!value) {
        size = g_strdup("0");

        return size;
    }

    switch (options->blocks[0]) {
    case KILOBYTE:
        size = g_strdup_printf("%li", value);
        break;
    case MEGABYTE:
        test = value / KBLOCK;
        if ((test - (gfloat) floor((gdouble) test)) >= 0.01)
            size = g_strdup_printf("%.2f", test);
        else
            size = g_strdup_printf("%.0f", test);
        break;
    case GIGABYTE:
        test = value / (KBLOCK * KBLOCK);
        if ((test - (gfloat) floor((gdouble) test)) >= 0.01)
            size = g_strdup_printf("%.2f", test);
        else
            size = g_strdup_printf("%.0f", test);
        break;
    default:
        options->blocks[0] = AUTOSIZE;
    case AUTOSIZE:
        if (value > KBLOCK * KBLOCK * KBLOCK) {
            test = value / (KBLOCK * KBLOCK * KBLOCK);
            if ((test - (gfloat) floor((gdouble) test)) >= 0.01)
                size = g_strdup_printf("%.2f T", test);
            else
                size = g_strdup_printf("%.0f T", test);
        } else if (value > KBLOCK * KBLOCK) {
            test = value / (KBLOCK * KBLOCK);
            if ((test - (gfloat) floor((gdouble) test)) >= 0.01)
                size = g_strdup_printf("%.2f G", test);
            else
                size = g_strdup_printf("%.0f G", test);
        } else if (value > KBLOCK) {
            test = value / KBLOCK;
            if ((test - (gfloat) floor((gdouble) test)) >= 0.01)
                size = g_strdup_printf("%.2f M", test);
            else
                size = g_strdup_printf("%.0f M", test);
        } else
            size = g_strdup_printf("%li K", value);
        break;
    }

    return size;
}

/* Format treeview's strings according to filesystem_t */
void list_filesystem_info_fill_strings (filesystem_t *fs)
{
    fs->fs_size = list_filesystem_size(fs->size);
    fs->fs_used = list_filesystem_size(fs->used);
    fs->fs_free = list_filesystem_size(fs->free);
    fs->fs_pused = g_strdup_printf("%i %%", fs->pused);
    fs->fs_pfree = g_strdup_printf("%i %%", fs->pfree);

    return;
}

/* Size informations and more about filesystems according OS */
void list_filesystem_info (filesystem_t *fs)
{
#if defined HAVE_STATFS && (defined __FREEBSD__ || defined __NETBSD__ || defined __AIX__)
    struct statfs sts;

    if ((statfs(fs->fs_mntpt, &sts) == -1) || !sts.f_blocks)
#elif defined HAVE_STATVFS && (defined __SOLARIS__ || defined __LINUX__)
    struct statvfs sts;

    if ((statvfs(fs->fs_mntpt, &sts) == -1) || !sts.f_blocks)
#else
    INFO((_("Gtkdiskfree does not provide informations about your operating system yet.")));
    if(TRUE)
#endif
    {
        fs->real = FALSE;
        fs->size = 0;
        fs->used = 0;
        fs->free = 0;
        fs->pused = 100;
        fs->pfree = 0;
    } else {
        fs->real = TRUE;
        fs->size = (ULONG(sts.f_bsize) / 1024) * ULONG(sts.f_blocks);
        fs->used = (ULONG(sts.f_bsize) / 1024) * ULONG(sts.f_blocks - sts.f_bfree);
        fs->free = (ULONG(sts.f_bsize) / 1024) * ULONG(sts.f_bfree);
        fs->pused = list_filesystem_percent(fs->size, fs->used);
        fs->pfree = list_filesystem_percent(fs->size, fs->free);
    }
#if defined HAVE_STATFS && (defined __FREEBSD__ || defined __NETBSD__)
    fs->fs_name = g_strdup(sts->f_mntfromname);
    fs->fs_type = g_strdup(sts->f_fstypename);
# endif /* __FREEBSD__ || __NETBSD__ */

    list_filesystem_info_fill_strings(fs);
    PRINT_DEBUG(("-> having info real=%i size=%li used=%li free=%li pused=%i pfree=%i",
            fs->real, fs->size, sfs->used, fs->free, fs->pused, fs->pfree));

    return;
}

/* Query filesystem_t for device 'text' */
filesystem_t *list_search (GList *list, gchar *text)
{
    list = g_list_first(list);

    while (list != NULL) {
        if (!strcmp(FILESYSTEM(list->data)->fs_mntpt, text))
            return FILESYSTEM(list->data);

        list = list->next;
    }

    return NULL;
}

/* list filesystems in FSTAB_FILESYSTEMS (os dependant) */
GList *list_filesystems_not_mounted (void)
{
    filesystem_t *fs;
    GList *list = NULL;
#if defined HAVE_SETMNTENT && defined HAVE_GETMNTENT && (defined __LINUX__ || defined __SOLARIS4__)
    /*    Linux's routine is based on Solaris 4.1.3, since Solaris
        5.x this part of code is not supported by Solaris. */
    FILE *file;
    struct mntent *fstb = NULL;

    if ((file = setmntent(FSTAB_FILESYSTEMS, "r")) != NULL) {
        while ((fstb = getmntent(file)) != NULL) {
#elif defined HAVE_SETFSENT && defined HAVE_GETFSENT && defined HAVE_ENDFSENT
    /* This part of code should be supported by many OS, surely
        most of the *BSD family, AIX too. */
    struct fstab *fstb = NULL;

    if (!setfsent()) {
        while ((fstb = getfsent()) != NULL) {
#elif defined HAVE_GETVFSENT && defined __SOLARIS5__
    /* Part of code for Solaris 5.x and newer. */
    FILE *file;
    struct vfstab fstb;

    if ((file = fopen(FSTAB_FILESYSTEMS, "r")) != NULL) {
        while (!getvfsent(file, &fstb)) {
#else
    INFO((_("Unable to get informations about filesystems in fstab : none function supported.")));

    if (FALSE) {
        while (FALSE) {
#endif
            if (strcmp(fstb->mnt_dir, "swap") &&
                strcmp(fstb->mnt_type, "swap")) {
                fs = filesystem_new(FS_UMOUNTED);
                fs->real = TRUE;
#if defined HAVE_SETMNTENT && defined HAVE_GETMNTENT && (defined __LINUX__ || defined __SOLARIS4__)
                fs->fs_name = g_strdup(fstb->mnt_fsname);
                fs->fs_type = g_strdup(fstb->mnt_type);
                fs->fs_options = g_strdup(fstb->mnt_opts);
                fs->fs_mntpt = g_strdup(fstb->mnt_dir);
#elif defined HAVE_SETFSENT && defined HAVE_GETFSENT && defined HAVE_ENDFSENT
                fs->fs_name = g_strdup(fstb->fs_spec);
                fs->fs_type = g_strdup(fstb->fs_vfstype);
                fs->fs_options = g_strdup(fstb->fs_mntops);
                fs->fs_mntpt = g_strdup(fstb->fs_file);
#elif defined HAVE_GETVFSENT && defined __SOLARIS5__
                fs->fs_name = g_strdup(fstb.vfs_special);
                if (fstb.vfs_mountp != NULL)
                    fil->fs_mntpt = g_strdup(fstb.vfs_mountp);
                else
                    fil->fs_mntpt = g_strdup(_("unknown"));
                fs->fs_type = g_strdup(fstb.vfs_vfstype);
                fs->fs_options = g_strdup(fstb.vfs_mntops);
#endif
                fs->size = 0;
                fs->used = 0;
                fs->free = 0;
                fs->pused = 100;
                fs->pfree = 0;
                list_filesystem_info_fill_strings(fs);

                list = g_list_append(list, fs);
                PRINT_DEBUG(("Added %s (supposed mount point %s) to fs list",
                        fs->fs_name, fs->fs_mntpt));
            }
        }
#if defined HAVE_SETMNTENT && defined HAVE_GETMNTENT && (defined __LINUX__ || defined __SOLARIS4__)
        endmntent(file);
    } else
        ERROR((_("Unable to get filesystems list in fstab : setmntent() returns NULL.")));
#elif defined HAVE_SETFSENT && defined HAVE_GETFSENT && defined HAVE_ENDFSENT
        endfsent();
    } else
        ERROR((_("Unable to get filesystems list in fstab : setfsent() returns 0.")));
#elif defined HAVE_GETVFSENT && defined __SOLARIS5__
    } else
        ERROR((_("Unable to get filesystems list in %s : cannot open file."),
                FSTAB_FILESYSTEMS));
#else
    }
#endif

    return list;
}

/* Return list of mounted filesystems. This function has been rewrite
    a few time since gtkdiskfree exist, since version 1.7.1, parts of
    code run on other operating systems that Linux has been inspirated
    from 'df' and 'mount' command source code. */
GList *
list_filesystems_mounted (void)
{
    filesystem_t *fs;
    GList *list = NULL;
#if defined HAVE_SETMNTENT && defined HAVE_GETMNTENT &&    defined HAVE_ENDMNTENT && (defined __LINUX__ || defined __SOLARIS4__)
    /* getmnent Linux routine is based on Solaris 4.1.3, since
        Solaris 5.x this part of code is not supported by
        Solaris. */
    FILE *file;
    struct mntent *mnt = NULL;

    if ((file = setmntent(MTAB_FILESYSTEMS, "r")) != NULL) {
        while ((mnt = getmntent(file)) != NULL) {
            if (strcmp(mnt->mnt_dir, "swap") &&
                strcmp(mnt->mnt_type, "swap")) {
                fs = filesystem_new(FS_MOUNTED);
                fs->fs_name = g_strdup(mnt->mnt_fsname);
                fs->fs_type = g_strdup(mnt->mnt_type);
                fs->fs_options = g_strdup(mnt->mnt_opts);
                fs->fs_mntpt = g_strdup(mnt->mnt_dir);
                list_filesystem_info(fs);

                list = g_list_append(list, fs);
                PRINT_DEBUG(("Added %s (mount point %s) to fs list",
                        fs->fs_name, fs->fs_mntpt));
            }

        }
        endmntent(file);
    } else
        ERROR((_("Unable to get filesystems list in %s : cannot open file."), FSTAB_FILESYSTEMS));
#elif defined HAVE_SETVFSENT && defined HAVE_GETVFSENT && defined HAVE_ENDVFSENT
    /* This part of code should be supported by many OS, surely
        most of the *BSD family, AIX too. */
#ifdef __AIX__
    struct vfs_ent *mnt = NULL;
#else
    struct ovfsconf *mnt = NULL;
#endif /* __FREEBSD__ */

    setvfsent(0);
    while ((mnt = getvfsent()) != NULL) {
        if (strcmp(mnt->vfc_name, "swap")
            /* && strcmp(mnt->mnt_type, "swap")*/) {
            fs = filesystem_new(FS_MOUNTED);
#ifdef __AIX__
            fs->fs_name = g_strdup(mnt->vfsent_mnt_hlpr);
            fs->fs_type = g_strdup(mnt->vfsent_fs_hlpr);
#endif /* __AIX__ */
            fs->fs_options = g_strdup(_("not implemented"));
            fs->fs_mntpt = g_strdup(mnt->vfc_name);
            list_filesystem_info(fs);

            list = g_list_append(list, fs);
            PRINT_DEBUG(("Added %s (mount point %s) to fs list",
                        fs->fs_name, fs->fs_mntpt));
        }
    }
    endvfsent();
#elif !defined HAVE_SETMNTENT && defined HAVE_GETMNTENT && !defined HAVE_ENDMNTENT && defined __SOLARIS__
    /* Solaris 5.x and newer routine. */
    FILE *file;
    struct mnttab mnt;

    if ((file = fopen(MTAB_FILESYSTEMS, "r")) != NULL) {
        while (!getmntent(file, &mnt)) {
            if (strcmp(mnt.mnt_mountp, "swap") &&
                strcmp(mnt.mnt_fstype, "swap")) {
                fs = filesystem_new(FS_MOUNTED);
                fs->fs_mntpt = g_strdup(mnt.mnt_mountp);
                fs->fs_name = g_strdup(mnt.mnt_special);
                fs->fs_type = g_strdup(mnt.mnt_fstype);
                fs->fs_options = g_strdup(mnt.mnt_mntopts);
                list_filesystem_info(fs);

                list = g_list_append(list, fs);
                PRINT_DEBUG(("Added %s (mount point %s) to fs list",
                        fs->fs_name, fs->fs_mntpt));
            }
        }
        fclose(file);
    } else
        ERROR((_("Unable to get informations about filesystems in %s : cannot open file."), MTAB_FILESYSTEMS));
#else
    INFO((_("Unable to get informations about filesystems in mtab : none function supported.")));
#endif /* __ALL__ */

    return list;
}

gboolean check_skip_fs(filesystem_t *fs) {
    GList *it_s = options->skip_fs;
    /* Iterate through masked fs type*/
    while(it_s) {
        /* And if current one is */
        if(strcmp(it_s->data, fs->fs_type) == 0) {
            /* Filesystem masked */
            return TRUE;
        }
        it_s = it_s->next;
    }
    /* Filesystem not masked */
    return FALSE;
}

gboolean check_mountpoint_force(filesystem_t *fs) {
    GList *it_f = options->mountpoint_force;
    /* Iterate through forced mountpoint */
    while(it_f) {
        /* If this entry mountpoint is forced */
        if(strcmp(it_f->data, fs->fs_mntpt) == 0)
            /* Then do not mask */
            return TRUE;
        it_f = it_f->next;
    }
    /* Filesystem masked */
    return FALSE;
}

gboolean check_entry(filesystem_t *fs) {
    return(!check_skip_fs(fs) || check_mountpoint_force(fs));
}

GList *
list_filesystems_merge (GList *l1, GList *l2)
{
    GList *list = NULL, *tmp = l1;
    filesystem_t *fs;

    if (l1 != NULL) {
        l1 = g_list_first(l1);
        while (l1 != NULL) {
            fs = FILESYSTEM(l1->data);
            if ((!fs->real && options->blocks[1]) || fs->real) {
                if(check_entry(fs)) {
                    fs->path = gui_list_find_row(fs->fs_mntpt);
                    fs->need_update = gui_list_row_needs_update(fs);
                    list = g_list_append(list, fs);
                    PRINT_DEBUG(("Keep %s (on %s)", fs->fs_name, fs->fs_mnpt));
                }
            }
            l1 = l1->next;
        }
    }

    if (l2 != NULL) {
        l2 = g_list_first(l2);
        while (l2 != NULL) {
            fs = list_search(tmp, FILESYSTEM(l2->data)->fs_mntpt);
            if (fs == NULL) {
                fs = FILESYSTEM(l2->data);
                if(check_entry(fs)) {
                    fs->path = gui_list_find_row(fs->fs_mntpt);
                    fs->need_update = gui_list_row_needs_update(fs);
                    list = g_list_append(list, fs);
                    PRINT_DEBUG(("Keep %s (on %s)", fs->fs_name, fs->fs_mnpt));
                }
            }
            l2 = l2->next;
        }
    }

    return list;
}

filesystem_t*
list_total_filesystems (GList *list)
{
    filesystem_t *fs = filesystem_new(FS_MOUNTED), *l = NULL;

    fs->real = TRUE;
    fs->path = gui_list_find_row(_("None"));
    if (fs->path != NULL)
        fs->need_update = TRUE;
    else
        fs->need_update = FALSE;

    fs->size = 0;
    fs->used = 0;
    fs->free = 0;
    list = g_list_first(list);
    while (list != NULL) {
        l = FILESYSTEM(list->data);
        fs->size += l->size;
        fs->used += l->used;
        fs->free += l->free;
        if (l->need_update)
            fs->need_update = TRUE;
        list = list->next;
    }
    fs->pused = list_filesystem_percent(fs->size, fs->used);
    fs->pfree = list_filesystem_percent(fs->size, fs->free);

    fs->fs_options = g_strdup(_("None"));
    fs->fs_type = g_strdup(_("None"));
    fs->fs_name = g_strdup(_("All mounted"));
    fs->fs_mntpt = g_strdup(_("None"));
    list_filesystem_info_fill_strings(fs);

    return fs;
}

void
list_filesystems (void)
{
    filesystem_t *total;
    GList *list = NULL;
    GList *l1 = NULL, *l2 = NULL;

    /* User want to see not mounted filesystems ? */
    PRINT_DEBUG(("Adding mounted fs"));
    l1 = list_filesystems_mounted();
    if (options->blocks[2]) {
        PRINT_DEBUG(("Adding not mounted fs"));
        l2 = list_filesystems_not_mounted();
    }
    PRINT_DEBUG(("Merging lists"));
    if (options->blocks[3]) {
        total = list_total_filesystems(l1);
        l1 = g_list_append(l1, total);
    }

    list = list_filesystems_merge(l1, l2);

    /*  We set the STATUS case of all lines to 0, then if a line
        isn't update by putting this value to 1, she will be
        removed. */
    gtk_tree_model_foreach(list_treemodel,
                    (GtkTreeModelForeachFunc)gui_list_set_line_removable,
                    NULL);

    /* We fill the treeview. */
    if (list != NULL) {
        /* TODO Find the leak */
        gui_list_capacities_init(list);
        g_list_foreach(list, (GFunc)gui_list_pre_update, NULL);
    }

    gui_list_remove_old_lines();

    /* Finaly we free the memory allocated by the list. */
    g_list_free(list);
    if (l1 != NULL) {
        g_list_foreach(l1, (GFunc)filesystem_free, NULL);
        g_list_free(l1);
    }
    if (l2 != NULL) {
        g_list_foreach(l2, (GFunc)filesystem_free, NULL);
        g_list_free(l2);
    }

    return;
}

gboolean
gui_list_main_update (GtkTreeView *list)
{
    list_filesystems();

    /* Resize window if needed */
    if (gui_list_updated) {
        if (options->column_style[0])
            gtk_tree_view_columns_autosize(GTK_TREE_VIEW(list_treeview));
        gui_main_window_resize();
        gui_list_updated = FALSE;
    }
    gui_color_update = FALSE;

    return TRUE;
}

