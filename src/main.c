/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "configure.h"
#include "interface.h"
#include "menus.h"

int main(int argc, char **argv)
{
    PRINT_DEBUG(("Compiled on %s", OS_TYPE));
    PRINT_DEBUG(("Or on %s", UOS_TYPE));

/*#ifdef ENABLE_NLS
    setlocale(LC_ALL, "");
    setlocale(LC_NUMERIC, "C");
    bind_textdomain_codeset(PACKAGE, "UTF-8");
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);
#endif*/

    /* Load config from file (if exists else takes default) and
     * from command line. */
    cfg_init(argc, argv);

    /* GTK+ initialisation */
    gtk_init(&argc, &argv);
//#ifdef ENABLE_NLS
    setlocale(LC_NUMERIC, "C"); // Why do gtk_init reset this
//#endif

    /* gtkdiskfree main window */
    gui_main_window_create();

    /* Load timeout to update filesystems informations every
     * options->update_interval. */
    gui_statusbar_timeout_update(NULL, REFRESH);

    /* Popup, etc... */
    /* gui_popup_create(); */

    /* GTK+ main */
    gtk_main();

    /* When we stop gtkdiskfree, we save current configuration. */
    cfgfil_write(options);

    return 0;
}
