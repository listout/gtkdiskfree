/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#ifndef GTKDISKFREE_DISKFREE_H
#define GTKDISKFREE_DISKFREE_H
#define KBLOCK     1024.0
#define ULONG(arg) ((gulong)arg)
#include <gtk/gtk.h>
#include "main.h"

/* On some OS like Linux or Solaris, we need to know the name of files
   which contain list of mounted and not mounted filesystems to get
   informations about it. Note that most of *BSD do not use file to
   store list of mounted fs. */
#if defined  __LINUX__ || defined __SOLARIS4__
# define MTAB_FILESYSTEMS "/etc/mtab"
# define FSTAB_FILESYSTEMS "/etc/fstab"
#endif

#ifdef __SOLARIS5__
# define MTAB_FILESYSTEMS         "/etc/mnttab"
# define FSTAB_FILESYSTEMS        "/etc/vfstab"
#endif

#define FILESYSTEM(arg) ((filesystem_t *)arg)

typedef struct
{
    /* Informations */
    gint new;
    gint mounted;
    gint real;
    gint need_update;

    /* Refer to an exitant line if exist */
    GtkTreePath *path;

    /* Size, free, used */
    gulong size;
    gulong used;
    gulong free;
    /* percent */
    gint pused;
    gint pfree;

    /* Displayed strings */
    gchar *fs_name;
    gchar *fs_mntpt;
    gchar *fs_options;
    gchar *fs_type;
    gchar *fs_size;
    gchar *fs_used;
    gchar *fs_free;
    gchar *fs_pused;
    gchar *fs_pfree;

    /* Pixbuf */
    GdkPixbuf *mount;
    GdkPixbuf *pixbuf;
} filesystem_t;

gboolean gui_list_main_update (GtkTreeView *);
#endif // GTKDISKFREE_DISKFREE_H
