/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "utils.h"

#ifdef STDC_HEADERS
#include <string.h>
#include <stdio.h>
#endif

#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif

#define NONE_FILE "none"

gint
filutils_get_type (const gchar file[])
{
    struct stat st;

    if (stat(file, &st) != 0)
        return NOEXIST_TYPE;
    if (S_IFDIR <= st.st_mode && S_IFBLK >= st.st_mode)
        return DIRECTORY_TYPE;
    else if (S_IFREG <= st.st_mode && S_IFLNK >= st.st_mode)
        return FILE_TYPE;

    return UNKNOW_TYPE;
}

gchar **
filutils_get_lines (const gchar file[])
{
    FILE *f;
    gchar *buffer, **lines = NULL;
    struct stat st;

    if (!strcmp(file, NONE_FILE))
        return lines;

    if (strstr(file, DIR_CHAR) == NULL) {
        ERROR((_("Could not open file %s"), file));

        return lines;
    }

    if ((f = fopen(file, "r")) == NULL) {
        ERROR((_("Could not open file %s"), file));

        return lines;
    }

    if (stat(file, &st) != 0) {
        ERROR((_("Could not get size for file %s"), file));
        fclose(f);

        return lines;
    }
    buffer = g_malloc(st.st_size + 1);
    if(fread(buffer, st.st_size, 1, f) == 1) {
        buffer[st.st_size] = '\0';
        lines = g_strsplit(buffer, "\n", 0);
    }
    else {
        ERROR((_("Could not read file %s"), file));
    }

    fclose(f);
    g_free(buffer);

    return lines;
}

gint
strutils_count_char (const gchar line[], gchar c)
{
    gint i, n;

    n = 0;
    i = 0;
    while (line[i] != '\0') {
        if (line[i] == c)
            n++;
        i++;
    }

    return n;
}

gchar *
strutils_get_arg (gchar *lines[], const gchar def[], const gchar del[])
{
    gint i;
    gchar **escape, *ret = NULL;

    /* Search line */
    i = 0;
    while (lines[i] != NULL &&
            strstr(lines[i], def) == NULL)
        i++;

    if (lines[i] != NULL) {
        escape = g_strsplit(lines[i], del, 2);
        ret = g_strdup(g_strstrip(escape[1]));
        g_strfreev(escape);
    }

    return ret;
}

GList * strutils_strdelim_to_glist(const gchar *arg, const gchar *del) {
    GList *ret;
    gchar **vector;
    gchar **it;

    vector = g_strsplit(arg, del, -1);
    if(vector == NULL)
        return NULL;

    it = vector;
    ret = NULL;
    while(*it) {
        ret = g_list_prepend(ret, g_strdup(*it));
        it++;
    }
    g_strfreev(vector);

    return g_list_reverse(ret);
}

/* TODO reshape this */
gchar * strutils_glist_concat(GList *list, const gchar *del) {
    if(list == NULL)
        return NULL;
    gchar * ret = g_strdup(list->data);
    list = list->next;
    while(list != NULL) {
        gchar *tmp;
        tmp=g_strjoin(del, ret, list->data, NULL);
        g_free(ret);
        ret=tmp;
        list = list->next;
    }
    return ret;
}

gboolean open_cmdtube (const gchar *cmd, const gchar *mount_point, gchar **error)
{
    gchar *line, *status;
    FILE *sh;

    setbuf(stdout, *error);
    line = g_strconcat(cmd, " ", mount_point, " 2>&1", NULL);
    sh = popen(line, "r");
    g_free(line);

    status = fgets(*error, MAXLINE-1, sh);

    if (status && (pclose(sh) != 0))
        return FALSE;

    return TRUE;
}
