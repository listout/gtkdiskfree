/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "pixmap.h"
#include "configure.h"

#ifdef HAVE_MATH_H
# include <math.h>
#endif

#if GTK_CHECK_VERSION(3, 0, 0) || G_BYTE_ORDER == G_BIG_ENDIAN /* ARGB */
    #define RED   0
    #define GREEN 1
    #define BLUE  2
#elif G_BYTE_ORDER == G_LITTLE_ENDIAN /* BGRA */
    #define GREEN 1
    #define RED   2
    #define BLUE  0
#else /* PDP endianness: RABG */
    #define RED   0
    #define GREEN 2
    #define BLUE  1
#endif

extern GtkWidget *mwindow;
double growth_p[100] = { 820, 820, 818, 818, 813, 811, 801, 803, 797, 792, 796, 791, 792, 785,
    780, 772, 772, 769, 759, 758, 748, 754, 734, 739, 731, 722, 719, 709, 702, 698, 697, 679,
    679, 668, 666, 650, 640, 640, 631, 620, 606, 607, 592, 579, 572, 567, 547, 541, 530, 529,
    511, 495, 493, 477, 463, 457, 438, 430, 418, 406, 393, 382, 370, 352, 336, 343, 310, 297,
    289, 283, 270, 250, 237, 228, 218, 201, 192, 176, 172, 158, 150, 138, 130, 119, 109, 101,
    91, 86, 72, 68, 63, 57, 48, 42, 39, 30, 27, 23, 19, 16 };

double color_white[3] = {1.1, 1.1, 1.1};

void pixmap_init_background(cairo_t *cr, cairo_surface_t *sr, const double *color) {
    cairo_set_source_rgb(cr, color[RED], color[GREEN], color[BLUE]);
    cairo_rectangle(cr, 0.0, 0.0, cairo_image_surface_get_width(sr), cairo_image_surface_get_height(sr));
    cairo_fill(cr);
    return;
}

cairo_t *cairo_context_init(cairo_surface_t **sr, GdkPixbuf **pixbuf, const double *color, gint width, gint height) {
    if(*pixbuf) {
        width = gdk_pixbuf_get_width(*pixbuf);
        height = gdk_pixbuf_get_height(*pixbuf);
    }

    cairo_t *ret;
#if GTK_CHECK_VERSION(3,0,0)
    if(*pixbuf)
        g_object_unref(G_OBJECT(*pixbuf));
    *sr = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    ret = cairo_create(*sr);
#else
    if(!*pixbuf)
        *pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);
    *sr = cairo_image_surface_create_for_data( gdk_pixbuf_get_pixels(*pixbuf),
                                CAIRO_FORMAT_ARGB32, width, height,
                                gdk_pixbuf_get_rowstride(*pixbuf));
    ret = cairo_create(*sr);
#endif
    if(color)
        pixmap_init_background(ret, *sr, color);
    return ret;
}

void cairo_context_finalize(cairo_t *cr, cairo_surface_t *sr, GdkPixbuf **pixbuf) {
#if GTK_CHECK_VERSION(3,0,0)
    *pixbuf=gdk_pixbuf_get_from_surface (sr, 0, 0, cairo_image_surface_get_width(sr), cairo_image_surface_get_height(sr));
#endif
    cairo_surface_destroy(sr);
    cairo_destroy(cr);
}

void fill_pixbuf_color(const gdouble *color, GdkPixbuf **pixbuf, int width, int height) {
    cairo_surface_t *sr;
    cairo_t *cr;

    cr = cairo_context_init(&sr, pixbuf, color, width, height);
    cairo_context_finalize(cr, sr, pixbuf);
}

GdkPixbuf *pixmap_draw_capacity (gint real, gint mounted, gint usedp, gint width)
{
    GdkPixbuf *pixbuf = NULL;
    double x, y, max;
    gint height = 12;
    gchar *text;

    cairo_text_extents_t extents;
    cairo_surface_t *surface;
    cairo_pattern_t *pattern;
    cairo_t *cr;

    max = (double)width / 100.0;
    max = max * usedp;

    cr = cairo_context_init(&surface, &pixbuf, color_white, width, height);

    /* Foreground color. */
    if (!real || mounted == FS_UMOUNTED) {
        cairo_set_source_rgb(cr, options->color3[RED], options->color3[GREEN], options->color3[BLUE]);
        cairo_rectangle(cr, 0.0, 0.0, width, height);
        cairo_fill(cr);
    } else if (options->capacity_style[0]) {
        pattern = cairo_pattern_create_linear(0.0, 0.0, width, 0.0);
        cairo_pattern_add_color_stop_rgba(pattern, 0.0, options->color1[RED], options->color1[GREEN], options->color1[BLUE], 1);
        cairo_pattern_add_color_stop_rgba(pattern, 1.0, options->color2[RED], options->color2[GREEN], options->color2[BLUE], 1);
        cairo_rectangle(cr, 0.0, 0.0, max, height);
        cairo_set_source(cr, pattern);
        cairo_fill(cr);
        cairo_pattern_destroy(pattern);
    } else {
        gdouble faded_color[3];
        int idx;
        for(idx=0; idx < 3; idx++)
            faded_color[idx] = ( options->color1[idx] * (gdouble)(100 - usedp) ) / 100.0 + ( options->color2[idx] * (gdouble)usedp ) / 100.0;
        cairo_set_source_rgb(cr, faded_color[RED], faded_color[GREEN], faded_color[BLUE]);

        cairo_rectangle(cr, 0.0, 0.0, max, height);
        cairo_fill(cr);
    }

    /* Add text in capacity column ? */
    if (options->capacity_style[1] && mounted == FS_MOUNTED && real) {
        text = g_strdup_printf("%i %%", usedp);
        cairo_set_source_rgb(cr, options->color_text[RED], options->color_text[GREEN], options->color_text[BLUE]);
        cairo_set_font_size(cr, 8);
        cairo_text_extents(cr, text, &extents);
        x = width / 2 - (extents.width / 2 + extents.x_bearing);
        y = height / 2 - (extents.height / 2 + extents.y_bearing);
        cairo_move_to(cr, x, y);
        cairo_show_text(cr, text);
        g_free(text);
    }

    cairo_rectangle(cr, 0.0, 0.0, width, height);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_set_line_width(cr, 1.0);
    cairo_stroke(cr);

    cairo_context_finalize(cr, surface, &pixbuf);

    return pixbuf;
}

void legend_draw(cairo_t *cr, const gchar *text, const gdouble *color, const double x, const double y)
{
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, x, y, 10.0, 10.0);
    cairo_stroke(cr);
    cairo_set_source_rgb(cr, color[RED], color[GREEN], color[BLUE]);
    cairo_rectangle(cr, x, y, 10.0, 10.0);
    cairo_fill(cr);
    cairo_set_source_rgb(cr, options->color_text[RED], options->color_text[GREEN], options->color_text[BLUE]);
    cairo_move_to(cr, x + 16.0, y + 8.0);
    cairo_show_text(cr, text);
}

GdkPixbuf *pixmap_draw_legend(GtkWidget *top, gint width, gint height) {
    GdkPixbuf *pixbuf = NULL;
    cairo_surface_t *surface;
    cairo_t *cr;

    cr = cairo_context_init(&surface, &pixbuf, color_white, width, height);

    cairo_set_line_width(cr, 1.0);

    legend_draw(cr, _("Free"), options->color1, 8.0, 2.0);
    legend_draw(cr, _("Used"), options->color2, (double)width / 2.0 + 8.0, 2.0);

    cairo_context_finalize(cr, surface, &pixbuf);

    return pixbuf;
}

void interpolate_color(cairo_t *cr, double *w_color, int idx, int limit) {
    double i_color[3];   /* Interpolated color */
    double *color;
    double ipol;
    int i;

    /* No interpolation */
    if(!options->popup[2]) {
        if(idx > limit)
            cairo_set_source_rgb(cr, options->color1[RED], options->color1[GREEN], options->color1[BLUE]);
        else
            cairo_set_source_rgb(cr, options->color2[RED], options->color2[GREEN], options->color2[BLUE]);
        return;
    }

    /* Interpolate_color */
    if(idx > limit) {
        /* progression in free range */
        ipol = (double)(100 - idx)/(double)(100 - limit);
        color = options->color1;
    }
    else {
        /* progression in used range */
        ipol = (double)(idx)/(double)(limit);
        ipol = ipol * ipol;
        color = options->color2;
    }
    ipol = ipol * ipol;
    for(i = 0; i < 3; i++)
        i_color[i] = ipol * w_color[i] + ( 1.0 - ipol ) * color[i];

    cairo_set_source_rgb(cr, i_color[RED], i_color[GREEN], i_color[BLUE]);

    return;
}

GdkPixbuf *pixmap_draw_fs_snail_chart(GtkWidget *top, gint size, gint total, gint ufree, gint sfree) {
    static gboolean init_done = FALSE;

    GdkPixbuf *pixbuf = NULL;
    cairo_surface_t *surface;
    cairo_t *cr;

    double angle = 6.0 * M_PI + M_PI_2;
    double step = 0.05 * M_PI;
    if(options->popup[0] == 3) step = 1.0 / 7.0 * M_PI;
    double radius = (double)size / 6.0;
    double r_step = radius * 0.00125; /* factor <= 0.005 (1/200) */
    double r_step_add = (radius - r_step * 100.0 - 1.2) / 5050.0;
    double center = (double)size * 0.5;
    double line_size = (double)size * 0.01;
    double w_color[3]; /* Weighted limit color */
    double x, y;
    gint limit;
    int idx, i;

    if(!init_done) {
        /* Count */
        for(y = 0, i = 0; i < 100; i++)
            y += growth_p[i];

        x = 0;
        for(i = 99; i >= 0; i--) {
            x = x + growth_p[i];
            growth_p[i] = (x * 100.0) / y;
        }

        init_done = TRUE;
    }


    cr = cairo_context_init(&surface, &pixbuf, color_white, size, size);

    x = (double)(total - ufree - sfree) / (double)total;
    if(options->popup[2])
        for(i = 0; i < 3; i++)
            w_color[i] = (1.0 - x) * options->color1[i] + x * options->color2[i];

    limit = x * 100.0;
    for(i=0; growth_p[i] > limit; i++);
    limit = 100 - i;
    x = limit / 100;

    for(idx=100; idx > 0; idx--) {
        x = center * ( 1.0 + 0.006 * (double)idx * cos(angle) );
        y = center * ( 1.0 + 0.006 * (double)idx * sin(angle) );

        cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, (double)idx * 0.005 + 0.5);
        cairo_arc (cr, x, y, radius, 0, M_PI + M_PI);
        cairo_set_line_width(cr, line_size);
        cairo_stroke_preserve(cr);

        interpolate_color(cr, w_color, idx, limit);
        cairo_fill(cr);

        angle = angle - step;
        radius = radius - r_step;
        r_step = r_step + r_step_add;
        line_size = line_size * 0.975;
    }

    cairo_context_finalize(cr, surface, &pixbuf);

    return pixbuf;
}

void pie_chart_draw_slice(cairo_t *cr, const gchar *legend, gdouble *color, double angle1, double angle2, double size) {
    cairo_text_extents_t extents;
    double center = size * 0.5;
    double radius = center * 0.95;
    double x, y;

    cairo_set_source_rgb(cr, color[RED], color[GREEN], color[BLUE]);
    cairo_move_to(cr, center, center);
    cairo_arc (cr, center, center, radius, angle1, angle2);
    cairo_fill(cr);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_text_extents(cr, legend, &extents);
    x = center * ( 1.0 + 0.6 * cos(angle2/2.0) );
    y = center * ( 1.0 + 0.6 * sin(angle2/2.0) );
    x = x - (extents.width / 2.0 + extents.x_bearing);
    y = y - (extents.height / 2.0 + extents.y_bearing);
    cairo_move_to(cr, x, y);
    cairo_show_text(cr, legend);
    cairo_move_to(cr, center, center);
    cairo_arc (cr, center, center, radius, angle1, angle2);
    cairo_set_line_width(cr, 1.0);
    cairo_stroke(cr);
}

GdkPixbuf *pixmap_draw_fs_pie_chart(GtkWidget *top, gint size, gint total, gint ufree, gint sfree) {
    GdkPixbuf *pixbuf = NULL;
    double angle;

    cairo_surface_t *surface;
    cairo_t *cr;

    cr = cairo_context_init(&surface, &pixbuf, color_white, size, size);

    angle = (M_PI + M_PI) * (double)(total - ufree - sfree) / (double)total;
    pie_chart_draw_slice(cr, _("Used"), options->color2, 0, angle, (double)size);
    pie_chart_draw_slice(cr, _("Free"), options->color1, angle, M_PI + M_PI, (double)size);

    cairo_context_finalize(cr, surface, &pixbuf);

    return pixbuf;
}

GdkPixbuf *pixmap_draw_mount(GtkWidget *top, gint size, gdouble *color) {
    GdkPixbuf *pixbuf = NULL;
    cairo_pattern_t *pat;
    cairo_surface_t *sr;
    cairo_t *cr;

    double x, y;
    double radius;

    /* TODO gtk2 get rid of transparent background artefacts */
    cr = cairo_context_init(&sr, &pixbuf, NULL, size, size);

    x = (double)size * 0.5;
    y = (double)size * 0.3075;
    radius = x * 0.95;

    pat = cairo_pattern_create_radial (x , x, 0.65, y, y, radius);
    cairo_pattern_add_color_stop_rgba (pat, 0.0 , color[RED], color[GREEN], color[BLUE], 1);
    cairo_pattern_add_color_stop_rgba (pat, 1.0 , 0.0, 0.0, 0.0, 1.0);
    cairo_set_source (cr, pat);
    cairo_arc (cr, x, x, radius, 0, M_PI + M_PI);
    cairo_fill (cr);

    cairo_context_finalize(cr, sr, &pixbuf);
    cairo_pattern_destroy(pat);

    return pixbuf;
}

