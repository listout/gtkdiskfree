/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "options.h"
#include "configure.h"
#include "interface.h"
#include "pixmap.h"
#include "widgets.h"

extern GtkWidget *mwindow;
extern GdkPixbuf *logo;

typedef struct {
    prefs_t *prefs;
    GtkWidget *window;
    GtkWidget *time_entry;
    GtkWidget *mounts_cmd[2];
    GtkWidget *fstype[3];
} ui_options_t;

gint tree_current_index = 0;
GtkWidget *color_selector[4] = { NULL, NULL, NULL, NULL };
gboolean update_logo = FALSE;

/* Color selection window's */
void colorsel_window_achieve(GtkWidget *button, GtkWidget *colorsel) {
    gdouble *color;
    gdouble old_color[4];
    GdkPixbuf *pixbuf;
    GtkWidget *image;
#if !defined OLD_COLOR_SELECTOR && GTK_CHECK_VERSION(3, 0, 0)
    GdkRGBA gdk_color;
#else
    GdkColor gdk_color;
#endif
    int idx;

    color = g_object_get_data(G_OBJECT(button), "color");

    for(idx=0; idx<3; idx++)
        old_color[idx] = color[idx];

#if !defined OLD_COLOR_SELECTOR && GTK_CHECK_VERSION(3, 0, 0)
    gdk_color.red   = color[0];
    gdk_color.green = color[1];
    gdk_color.blue  = color[2];
    gdk_color.alpha = 1;
    gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(colorsel), &gdk_color);
#else
    GtkWidget *color_selection;
    color_selection = gtk_color_selection_dialog_get_color_selection(GTK_COLOR_SELECTION_DIALOG(colorsel));
    gdk_color.red   = color[0] * COLOR_MAX;
    gdk_color.green = color[1] * COLOR_MAX;
    gdk_color.blue  = color[2] * COLOR_MAX;
    gtk_color_selection_set_current_color(GTK_COLOR_SELECTION(color_selection), &gdk_color);
#endif

    gtk_dialog_run( GTK_DIALOG( colorsel ) );
    gtk_widget_hide( colorsel );

#if !defined OLD_COLOR_SELECTOR && GTK_CHECK_VERSION(3, 0, 0)
    gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(colorsel), &gdk_color);
    color[0] = gdk_color.red;
    color[1] = gdk_color.green;
    color[2] = gdk_color.blue;
#else
    gtk_color_selection_get_current_color(GTK_COLOR_SELECTION(color_selection), &gdk_color);
    color[0] = (double)gdk_color.red   / COLOR_MAX;
    color[1] = (double)gdk_color.green / COLOR_MAX;
    color[2] = (double)gdk_color.blue  / COLOR_MAX;
#endif

    for(idx=0; idx<3; idx++)
        if(old_color[idx] != color[idx])
            update_logo = TRUE;

    image = g_object_get_data(G_OBJECT(button), "pixmap");
    pixbuf = gtk_image_get_pixbuf(GTK_IMAGE(image));;
    fill_pixbuf_color(color, &pixbuf, PREVIEW_W, PREVIEW_H);
}

void colorsel_window_create(GtkWidget * button, gpointer data) {
    gint idx = GPOINTER_TO_INT(data);
    if(color_selector[idx] == NULL) {
        gchar *title;

        switch(idx) {
            case 0: title = _("Select first color"); break;
            case 1: title = _("Select second color"); break;
            case 2: title = _("Select text color"); break;
            case 3: title = _("Select unmounted color"); break;
            default: title = "";
        }
#if !defined OLD_COLOR_SELECTOR && GTK_CHECK_VERSION(3, 0, 0)
        color_selector[idx] = gtk_color_chooser_dialog_new (title, NULL);
#else
        color_selector[idx] = gtk_color_selection_dialog_new (title);
#endif
    }
    gtk_window_set_transient_for(GTK_WINDOW(color_selector[idx]),
            GTK_WINDOW(gtk_widget_get_ancestor(button, GTK_TYPE_WINDOW)));
    colorsel_window_achieve(button, color_selector[idx]);

    return;
}


void
options_window_get_entrys (ui_options_t *win)
{
    prefs_t *prefs = win->prefs;

    widget_entry_get_chars(win->mounts_cmd[0], &prefs->mount_cmds[0]);
    widget_entry_get_chars(win->mounts_cmd[1], &prefs->mount_cmds[1]);

    widget_entry_get_list(win->fstype[0], &prefs->skip_fs, LIST_CHAR);
    widget_entry_get_list(win->fstype[1], &prefs->mountpoint_force, LIST_CHAR);

    return;
}

void
options_ok_button_clicked (ui_options_t *win)
{
    gint i_snail_bk = options->popup[2];
    gint t_snail_bk = options->popup[0];

    options_window_get_entrys(win);
    cfg_cpy(options, win->prefs);

    gui_list_columns_disp_update();
    gui_main_window_disp_update();
    gui_main_window_resize();

    gui_statusbar_update_block();
    gui_statusbar_timeout_update(NULL, UPDATE);

    if(update_logo
            || i_snail_bk != options->popup[2]
            || t_snail_bk != options->popup[0] ) {
        if(logo) {
            g_object_unref(G_OBJECT(logo));
            logo = pixmap_draw_fs_snail_chart(mwindow, 100, 100, 50, 0);
        }
        update_logo = FALSE;
    }

    gui_color_update = TRUE;
    gui_list_capacities_redraw();

    return;
}

void notebook_set_page(GtkNotebook *notebook, GtkWidget *page, guint index, gpointer data) {
    tree_current_index = index;
}

ui_options_t *
options_new (void)
{
    ui_options_t *win;

    win = g_malloc(sizeof(ui_options_t));

    return win;
}

gboolean
ui_options_destroy (ui_options_t **win)
{
    ui_options_t *tmp = *win;

    cfg_free(tmp->prefs);
    if (tmp != NULL)
        g_free(tmp);
    *win = NULL;

    return TRUE;
}

GtkWidget *
ui_widget_tree_add_row (GtkNotebook *notebook, char *name)
{
    GtkWidget *vbox;
    GtkWidget *label;

    /* Notebook filling */
    GTK_BOX_V(vbox, FALSE, 8)
    label = gtk_label_new(name);
    gtk_notebook_append_page(notebook, vbox, label);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 8);
    gtk_widget_show(vbox);

    return vbox;
}

void ui_options_window_create ()
{
    static ui_options_t *win = NULL;
    gint response;

    if (win == NULL) {
        prefs_t *prefs = NULL;
        GtkWidget *h_box;
        GtkWidget *button;

        GtkWidget *notebook;
        GtkWidget *frame;
        GtkWidget *p_box;

        GtkWidget *vbox1;
        GtkWidget *hbox_columns;

        GtkWidget *check_button;
        GtkWidget *radio_button;

        win = options_new();
        win->prefs = cfg_new();
        cfg_cpy(win->prefs, options);
        prefs = win->prefs;

        win->window = gtk_dialog_new();
        gtk_window_set_transient_for(GTK_WINDOW(win->window), GTK_WINDOW(mwindow));
        gtk_window_set_resizable(GTK_WINDOW(win->window), FALSE);
        gtk_window_set_title(GTK_WINDOW(win->window), _("Properties"));
        gtk_widget_realize(GTK_WIDGET(win->window));
        gtk_container_set_border_width(GTK_CONTAINER(win->window), 5);
        g_signal_connect_swapped(G_OBJECT(win->window), "destroy",
                        G_CALLBACK(ui_options_destroy),
                        (gpointer)(&win));

        notebook = gtk_notebook_new();
        gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
        gtk_notebook_set_show_border(GTK_NOTEBOOK(notebook), TRUE);
        gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(win->window))), notebook, TRUE, TRUE, 0);
        gtk_widget_show(notebook);

        p_box = ui_widget_tree_add_row(GTK_NOTEBOOK(notebook), _("Rows"));

        frame = widget_frame_add(p_box, _("Filesystems to display"));
        widget_check_add(frame, &prefs->blocks[1],
                    _("Include filesystems contain 0 blocks"));
        widget_check_add(frame, &prefs->blocks[2],
                    _("Include not mounted filesystems"));
        widget_check_add(frame, &prefs->blocks[3],
                    _("Display a filesystem which is the total of all filesystems"));

        frame = widget_frame_add(p_box, _("Blocks size"));

        GTK_BOX_H(h_box, FALSE, 0)
        gtk_box_pack_start(GTK_BOX(frame), h_box, TRUE, TRUE, 0);
        gtk_widget_show(h_box);

        radio_button = widget_radio_add(h_box, NULL, AUTOSIZE, &prefs->blocks[0],
                        _("Human readable"));
        radio_button = widget_radio_add(h_box, radio_button, GIGABYTE, &prefs->blocks[0],
                        _("Gigabytes"));
        radio_button = widget_radio_add(h_box, radio_button, MEGABYTE, &prefs->blocks[0],
                        _("Megabytes"));
        radio_button = widget_radio_add(h_box, radio_button, KILOBYTE, &prefs->blocks[0],
                        _("Kilobytes"));

        frame = widget_frame_add(p_box, _("Update"));
        widget_spin_add_float(frame, &prefs->update_interval,
                        1.0, 900.0, 0.500, _("Interval :"));
        frame = widget_frame_add(p_box, _("Filters"));

        gchar * sample;
        sample = strutils_glist_concat(prefs->skip_fs, LIST_CHAR);
        win->fstype[0] = widget_entry_add_chars(frame, &sample, _("Ommitted types :"));
        sample = strutils_glist_concat(prefs->mountpoint_force, LIST_CHAR);
        win->fstype[1] = widget_entry_add_chars(frame, &sample, _("Forced mountpoints :"));
        gtk_widget_show(p_box);

        p_box = ui_widget_tree_add_row(GTK_NOTEBOOK(notebook), _("Interface"));

        frame = widget_frame_add(p_box, _("Main window style"));
        widget_check_add(frame, &prefs->gui_style[0], _("Show menubar"));
        widget_check_add(frame, &prefs->gui_style[1], _("Show toolbar"));
        widget_check_add(frame, &prefs->gui_style[2], _("Show statusbar"));
        widget_check_add(frame, &prefs->gui_style[3], _("Auto resize main window"));

        frame = widget_frame_add(p_box, _("Columns style"));
        widget_check_add(frame, &prefs->column_style[0], _("Optimal column resize"));
        widget_check_add(frame, &prefs->column_style[1], _("Allow columns resizing"));
        widget_check_add(frame, &prefs->column_style[2], _("Show clist titles"));
        widget_check_add(frame, &prefs->column_style[3], _("Use rules hint"));

        frame = widget_frame_add(p_box, _("Columns view"));

        GTK_BOX_H(hbox_columns, TRUE, 1)
        check_button = widget_check_add(frame, &prefs->show_columns_all,
                        _("Show all columns"));
        g_signal_connect_swapped(G_OBJECT(check_button), "toggled",
                        G_CALLBACK(widget_set_sensitivity_invert),
                        G_OBJECT(hbox_columns));
        gtk_box_pack_start(GTK_BOX(frame), hbox_columns, FALSE, FALSE, 0);
        gtk_widget_show(hbox_columns);

        if (prefs->show_columns_all)
            gtk_widget_set_sensitive(hbox_columns, FALSE);

        GTK_BOX_V(vbox1, FALSE, 1)
        gtk_box_pack_start(GTK_BOX(hbox_columns), vbox1, FALSE, FALSE, 0);
        gtk_widget_show(vbox1);

        widget_check_add(vbox1, &prefs->show_columns[FILESYSTEM_COLUMN],
                    _("Filesystem"));
        widget_check_add(vbox1, &prefs->show_columns[SIZE_COLUMN],
                    _("Size"));
        widget_check_add(vbox1, &prefs->show_columns[USED_SPACE_COLUMN],
                    _("Used space"));

        GTK_BOX_V(vbox1, FALSE, 1)
        gtk_box_pack_start(GTK_BOX(hbox_columns), vbox1, FALSE, FALSE, 0);
        gtk_widget_show(vbox1);

        widget_check_add(vbox1, &prefs->show_columns[FREE_SPACE_COLUMN],
                    _("Free space"));
        widget_check_add(vbox1, &prefs->show_columns[USED_PERCENT_COLUMN],
                    _("Used percent"));
        widget_check_add(vbox1, &prefs->show_columns[FREE_PERCENT_COLUMN],
                    _("Free percent"));

        GTK_BOX_V(vbox1, FALSE, 1)
        gtk_box_pack_start(GTK_BOX(hbox_columns), vbox1, FALSE, FALSE, 0);
        gtk_widget_show(vbox1);

        widget_check_add(vbox1, &prefs->show_columns[FILESYSTEM_TYPE_COLUMN],
                    _("Filesystem type"));
        widget_check_add(vbox1, &prefs->show_columns[MOUNT_OPTIONS_COLUMN],
                    _("Mount option's"));
        widget_check_add(vbox1, &prefs->show_columns[MOUNT_POINT_COLUMN],
                    _("Mountpoint"));

        GTK_BOX_V(vbox1, FALSE, 1)
        gtk_box_pack_start(GTK_BOX(hbox_columns), vbox1, FALSE, FALSE, 0);
        gtk_widget_show(vbox1);

        button = widget_check_add(vbox1, &prefs->show_columns[MOUNT_COLUMN],
                    _("Mount command"));
        widget_check_add(vbox1, &prefs->show_columns[CAPACITY_COLUMN],
                    _("Capacity"));
        gtk_widget_show(p_box);


        p_box = ui_widget_tree_add_row(GTK_NOTEBOOK(notebook), _("Capacity"));

        frame = widget_frame_add(p_box, _("Properties"));

        widget_check_add(frame, &prefs->capacity_style[0], _("Gradient"));
        widget_check_add(frame, &prefs->capacity_style[1], _("Show percent"));
        widget_check_add(frame, &prefs->capacity_style[2], _("Scale"));

        widget_spin_add_int(frame, &prefs->capacity_style[3],
                            8, 800, 4, _("Default width:"));

        frame = widget_frame_add(p_box, _("Colour"));

        GTK_BOX_H(h_box, TRUE, 1)
        gtk_box_pack_start(GTK_BOX(frame), h_box, TRUE, TRUE, 0);
        gtk_widget_show(h_box);

        widget_button_color_add(h_box, G_CALLBACK(colorsel_window_create),
                          0, prefs->color1, _("Begin :"));
        widget_button_color_add(h_box, G_CALLBACK(colorsel_window_create),
                          1, prefs->color2, _("End :"));
        widget_button_color_add(h_box, G_CALLBACK(colorsel_window_create),
                          2, prefs->color_text, _("Text :"));
        widget_button_color_add(h_box, G_CALLBACK(colorsel_window_create),
                          3, prefs->color3, _("Not mounted :"));

        frame = widget_frame_add(p_box, _("Capacity popup"));

        GTK_BOX_H(h_box, FALSE, 0)
        gtk_box_pack_start(GTK_BOX(frame), h_box, TRUE, TRUE, 0);
        gtk_widget_show(h_box);

        radio_button = widget_radio_add(h_box, NULL        , 0, &prefs->popup[0], _("None"));
        radio_button = widget_radio_add(h_box, radio_button, 1, &prefs->popup[0], _("Pie chart"));
        radio_button = widget_radio_add(h_box, radio_button, 2, &prefs->popup[0], _("Snail chart"));
        radio_button = widget_radio_add(h_box, radio_button, 3, &prefs->popup[0], _("Whelk chart"));

        GTK_BOX_H(h_box, FALSE, 0)
        gtk_box_pack_start(GTK_BOX(frame), h_box, TRUE, TRUE, 10);
        gtk_widget_show(h_box);

        widget_check_add(frame, &prefs->popup[2], _("Interpolate color in snail chart"));
        widget_check_add(frame, &prefs->popup[1], _("Show legend"));

        p_box = ui_widget_tree_add_row(GTK_NOTEBOOK(notebook), _("Commands"));

        frame = widget_frame_add(p_box, _("General"));
        win->mounts_cmd[0] = widget_entry_add_chars(frame, &prefs->mount_cmds[0], _("Mount :"));
        win->mounts_cmd[1] = widget_entry_add_chars(frame, &prefs->mount_cmds[1], _("Umount :"));

        gtk_dialog_add_button(GTK_DIALOG(win->window), _("Apply"), GTK_RESPONSE_APPLY);
        gtk_dialog_add_button(GTK_DIALOG(win->window), _("OK"), GTK_RESPONSE_OK);
        button = gtk_dialog_add_button (GTK_DIALOG(win->window), _("Cancel"), GTK_RESPONSE_CANCEL);
        gtk_widget_grab_default(button);

        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), tree_current_index);
        g_signal_connect(G_OBJECT(notebook), "switch-page",
                        G_CALLBACK(notebook_set_page),
                        NULL);
        gtk_widget_show_all(win->window);
    }

    while(1) {
        response = gtk_dialog_run(GTK_DIALOG(win->window));

        if(response == GTK_RESPONSE_OK || response == GTK_RESPONSE_APPLY)
            options_ok_button_clicked(win);
        if(response == GTK_RESPONSE_OK || response == GTK_RESPONSE_CANCEL)
            break;
        if(response != GTK_RESPONSE_APPLY)
            break;
    }
    gtk_widget_destroy(win->window);

    return;
}
