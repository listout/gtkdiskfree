/*
  GtkDiskFree shows free space on your mounted partitions.  Copyright
  (C) 2001-2002 Dj-Death (Landwerlin Lionel)
  (C) 2012-2016 mazes_80 (Bauer Samuel)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
  USA */

#include "configure.h"

#define CONFIG_FILENAME          "/gtkdiskfree3"
#define INFO(args) (printf("** gtkdiskfree info: "), printf args, printf(" **\n"))

#ifdef STDC_HEADERS
#include <stdio.h>
#include <stdlib.h>
#endif

#ifdef HAVE_GETOPT_H
# include <getopt.h>
#endif

#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif

#ifdef HAVE_ERRNO_H
# include <errno.h>
#endif

gint cfg_action = DEFAULT;
gchar **cfg_lines = NULL;
FILE *cfg_file = NULL;

prefs_t *options;

void
print_help (const gchar *myname)
{
#ifdef HAVE_GETOPT_LONG
    printf(_("USAGE : %s [OPTION]...\n"), myname);
    printf(_("\n"));
    printf(_("      -d, --default       load gtkdiskfree with default values\n"));
    printf(_("      -p, --position XxY  set gtkdiskfree's window position\n"));
    printf(_("         ex : %s -p 150x200\n"), myname);
    printf(_("      -s, --size WxH      set gtkdiskfree's window size\n"));
    printf(_("         ex : %s -s 300x150\n"), myname);
    printf(_("      -c, --columns       set gtkdiskfree's columns to display (%i columns)\n"),
            CAPACITY_COLUMN + 1);
    printf(_("         ex : %s -c 1111000011\n"), myname);
    printf(_("      -t, --time VALUE    look at filesystem every VALUE (seconds)\n"));
    printf(_("         ex : %s -t 10\n"), myname);
    printf(_("      -h, --help          print this screen\n"));
    printf(_("      -v, --version       print version of gtkdiskfree\n"));
    printf(_("Report any bug to <samuel.bauer@yahoo.fr>.\n"));
#elif HAVE_GETOPT
    printf(_("USAGE : %s [OPTION]...\n"), myname);
    printf(_("\n"));
    printf(_("      -d,       load gtkdiskfree with default values\n"));
    printf(_("      -p XxY    set gtkdiskfree's window position\n"));
    printf(_("         ex : %s -p 150x200\n"), myname);
    printf(_("      -s WxH    set gtkdiskfree's window size\n"));
    printf(_("         ex : %s -s 300x150\n"), myname);
    printf(_("      -c        set gtkdiskfree's columns to display (%i columns)\n"),
            CAPACITY_COLUMN + 1);
    printf(_("         ex : %s -c 1111000011\n"), myname);
    printf(_("      -t VALUE  look at filesystem every VALUE (seconds)\n"));
    printf(_("         ex : %s -t 10\n"), myname);
    printf(_("      -h        print this screen\n"));
    printf(_("      -v        print version of gtkdiskfree\n"));
    printf(_("Report any bug to <samuel.bauer@yahoo.fr>.\n"));
#else
    INFO((_("Options still not supported by your operating system.")));
    INFO((_("Needed GNU/POSIX getopt function.")));
#endif

    exit(0);
}

void
print_version (void)
{
    INFO(("%s", VERSION));

    exit(0);
}

void
cfgdir_create (void)
{
    const gchar *config_directory;
    gint ret;

    config_directory = g_get_user_config_dir();

#ifdef HAVE_MKDIR
    if (!(ret = mkdir(config_directory, 0700)))
        INFO((_("Config directory created.")));
    else {
# ifdef HAVE_ERRNO_H
        if (errno == EEXIST)
            INFO((_("Config directory already exist.")));
        else
            ERROR((_("Can't create config directory, error code %i."), errno));
# else
        ERROR((_("Can't create config directory, error code %i.")));
# endif
    }
#else
    INFO((_("Can't create config directory, you system needs mkdir function.")));
#endif
    return;
}

void
cfgfil_write_head (FILE *file)
{
    fprintf(file, "# GTKDISKFREE %s config file #\n", VERSION);
    fprintf(file, "# AUTOWRITTEN, DO NOT EDIT #\n\n");

    return;
}

void
cfgfil_rwd_val_str (const gchar *def, gchar **val, const gchar *deft)
{
    switch (cfg_action) {
        case READ:
            if (*val != NULL)
                g_free(*val);
            if ((*val = strutils_get_arg(cfg_lines, def, DEFCFG_CHAR)) != NULL)
                break;
        case DEFAULT:
        default:
            if (*val != NULL)
                g_free(*val);
            *val = g_strdup(deft);
            break;
        case WRITE:
            if (*val == NULL)
                return;
            fprintf(cfg_file, "%s %s %s\n", def, DEFCFG_CHAR, *val);
            break;
    }

    return;
}

void
cfgfil_rwd_val_list (const gchar *def, GList **val, const gchar *del, const gchar *deft)
{
    gchar *tmp;

    switch (cfg_action) {
        case READ:
            g_list_free_full(*val, g_free);
            *val = NULL; // do worry about next text
            tmp=strutils_get_arg(cfg_lines, def, DEFCFG_CHAR);
            if(tmp != NULL) {
                *val = strutils_strdelim_to_glist(tmp, del);
                g_free(tmp);
            }
            if (*val != NULL)
                break;
        case DEFAULT:
        default:
            g_list_free_full(*val, g_free);
            *val = strutils_strdelim_to_glist(deft, del);
            break;
        case WRITE:
            if (*val == NULL)
                return;
            tmp = strutils_glist_concat(*val, del);
            fprintf(cfg_file, "%s %s %s\n", def, DEFCFG_CHAR, tmp);
            g_free(tmp);
            break;
    }

    return;
}

void
cfgfil_rwd_val_float (const gchar *def, gfloat *val, const gchar *deft)
{
    gchar *arg;

    switch (cfg_action) {
        case READ:
            if ((arg = strutils_get_arg(cfg_lines, def, DEFCFG_CHAR)) != NULL) {
                *val = atof(arg);
                g_free(arg);
                break;
            }
        case DEFAULT:
        default:
            *val = atof(deft);
            break;
        case WRITE:
            fprintf(cfg_file, "%s %s %f\n", def, DEFCFG_CHAR, *val);
            break;
    }

    return;
}

void
cfgfil_rwd_val_int (const gchar *def, gint *val, const gchar *deft)
{
    gchar *arg;

    switch (cfg_action) {
        case READ:
            if ((arg = strutils_get_arg(cfg_lines, def, DEFCFG_CHAR)) != NULL) {
                *val = atoi(arg);
                g_free(arg);
                break;
            }
        case DEFAULT:
        default:
            *val = atoi(deft);
            break;
        case WRITE:
            fprintf(cfg_file, "%s %s %i\n", def, DEFCFG_CHAR, *val);
            break;
    }

    return;
}

void
cfgfil_rwd_val_bint (const gchar *def, gint *val, const gchar *del,
            gint n, const gchar *deft)
{
    gint i;
    gchar *arg, **board;
    GString *line;

    switch (cfg_action) {
        case READ:
            if ((arg = strutils_get_arg(cfg_lines, def, DEFCFG_CHAR)) != NULL) {
                board = g_strsplit(arg, del, n);
                g_free(arg);

                for (i = 0 ; i < n ; i++) {
                    if (board[i] == NULL)
                        break;
                    val[i] = atoi(board[i]);
                }
                g_strfreev(board);
                break;
            }
        case DEFAULT:
        default:
            board = g_strsplit(deft, del, n);
            for (i = 0 ; i < n ; i++)
                val[i] = atoi(board[i]);
            g_strfreev(board);
            break;
        case WRITE:
            line = g_string_new("");
            i = 0;
            while (i < n) {
                if (i != 0)
                    line = g_string_append(line, del);
                arg = g_strdup_printf("%i", val[i]);
                line = g_string_append(line, arg);
                g_free(arg);
                i++;
            }
            fprintf(cfg_file, "%s %s %s\n", def,
                DEFCFG_CHAR, line->str);
            g_string_free(line, TRUE);
            break;
    }

    return;
}

void
cfgfil_rwd_val_bdouble (const gchar *def, gdouble *val, const gchar *del,
            gint n, const gchar *deft)
{
    gint i;
    gchar *arg, **board;
    GString *line;

    switch (cfg_action) {
        case READ:
            if ((arg = strutils_get_arg(cfg_lines, def, DEFCFG_CHAR)) != NULL) {
                board = g_strsplit(arg, del, n);
                g_free(arg);

                for (i = 0 ; i < n ; i++) {
                    if (board[i] == NULL)
                        break;
                    val[i] = atof(board[i]);
                }
                g_strfreev(board);
                break;
            }
        case DEFAULT:
        default:
            board = g_strsplit(deft, del, n);
            for (i = 0 ; i < n ; i++)
                val[i] = atof(board[i]);
            g_strfreev(board);
            break;
        case WRITE:
            line = g_string_new("");
            i = 0;
            while (i < n) {
                if (i != 0)
                    line = g_string_append(line, del);
                arg = g_strdup_printf("%f", val[i]);
                line = g_string_append(line, arg);
                g_free(arg);
                i++;
            }
            fprintf(cfg_file, "%s %s %s\n",
                def,
                DEFCFG_CHAR, line->str);
            g_string_free(line, TRUE);
            break;
    }

    return;
}

void
cfgfil_rwd_values (prefs_t *pref)
{
    /* Window Position */
    cfgfil_rwd_val_bint("window_position", pref->window_position, X_CHAR, 2, "-1x-1");
    /* Window Size */
    cfgfil_rwd_val_bint("window_size", pref->window_size, X_CHAR, 2, "-1x-1");
    /* Block (size, virtual, not mounted, total) */
    cfgfil_rwd_val_bint("blocks", pref->blocks, DIR_CHAR, 4, "3/0/1/0");
    /* Sort */
    cfgfil_rwd_val_bint("sort_by", pref->sort, DIR_CHAR, 2, "2/1");
    /* GUI styl */
    cfgfil_rwd_val_bint("GUI_style", pref->gui_style, DIR_CHAR, 5, "0/1/1/1/1");
    /* Clist styl */
    cfgfil_rwd_val_bint("column_style", pref->column_style, DIR_CHAR, 4, "1/1/1/1");
    /* refreshment */
    cfgfil_rwd_val_float("update_interval", &pref->update_interval, "5.00000");
    /* Show all Columns */
    cfgfil_rwd_val_int("show_columns_all", &pref->show_columns_all, "0");
    /* Shown columns */
    cfgfil_rwd_val_bint("show_columns_detail", pref->show_columns,
                DIR_CHAR, CAPACITY_COLUMN + 1, "1/1/1/1/0/0/1/0/1/1/1");
    /* Color percent */
    cfgfil_rwd_val_bint("capacity_style", pref->capacity_style, DIR_CHAR, 4, "1/1/1/200");
    /* Color 1 */
    cfgfil_rwd_val_bdouble("color_start", pref->color1, DIR_CHAR, 3,
                    "0.786343/0.923919/0.457633");
    /* Color 2 */
    cfgfil_rwd_val_bdouble("color_stop", pref->color2, DIR_CHAR, 3,
                    "0.747872/0.373936/0.000000");
    /* Color text */
    cfgfil_rwd_val_bdouble("color_text", pref->color_text, DIR_CHAR, 3,
                    "0.000000/0.000000/0.400000");
    /* Color 3 */
    cfgfil_rwd_val_bdouble("color_unmounted", pref->color3, DIR_CHAR, 3,
                    "0.750000/0.750000/0.750000");
    /* Capacity popup settings */
    cfgfil_rwd_val_bint("popup", pref->popup, DIR_CHAR, 4, "2/1/1/20");
    /* Mount command */
    cfgfil_rwd_val_str("mount_cmd", &pref->mount_cmds[0], "mount");
    /* Umount command */
    cfgfil_rwd_val_str("umount_cmd", &pref->mount_cmds[1], "umount");
    /* Skipped filesystems */
    cfgfil_rwd_val_list("skip_fs", &pref->skip_fs, LIST_CHAR, "tmpfs;devtmpfs");
    /* Forced mountpoints */
    cfgfil_rwd_val_list("mountpoint_force", &pref->mountpoint_force, LIST_CHAR, "");

    return;
}

void
cfgfil_read (prefs_t *prefs)
{
    const gchar *config_directory;
    gchar *config_filename;

    cfg_action = READ;

    config_directory = g_get_user_config_dir();

    config_filename = g_strconcat(config_directory,
                        CONFIG_FILENAME,
                        NULL);

    if (filutils_get_type(config_filename) == NOEXIST_TYPE) {
        cfgdir_create();
        cfgfil_default(prefs);
        g_free(config_filename);

        return;
    }
    cfg_lines = filutils_get_lines(config_filename);
    g_free(config_filename);

    if (cfg_lines == NULL) {
        cfgfil_default(prefs);

        return;
    }
    cfgfil_rwd_values(prefs);

    if (cfg_lines != NULL)
        g_strfreev(cfg_lines);
    cfg_lines = NULL;
    cfg_action = DEFAULT;

    return;
}

void
cfgfil_write (prefs_t *pref)
{
    const gchar *config_directory;
    gchar *config_filename;

    cfg_action = WRITE;

    config_directory = g_get_user_config_dir();

    config_filename = g_strconcat(config_directory,
                        CONFIG_FILENAME,
                        NULL);

    if ((cfg_file = fopen(config_filename, "w")) == NULL) {
        ERROR((_("Cannot open %s !"), config_filename));
        cfgdir_create();
        if ((cfg_file = fopen(config_filename, "w")) == NULL) {
            ERROR((_("Please restart gtkdiskfree, it will work next time.")));

            return;
        }
    }
    g_free(config_filename);

    cfgfil_write_head(cfg_file);
    cfgfil_rwd_values(pref);

    fclose(cfg_file);
    cfg_action = DEFAULT;

    return;
}

void
cfg_init (gint argc, gchar **argv)
{
    gchar **position, **size, *columns = NULL;
    gint time, defaut, c, i;
#ifdef HAVE_GETOPT_LONG
    struct option long_options[] = {
        {"help", no_argument, NULL, 'h'},
        {"version", no_argument, NULL, 'v'},
        {"default", no_argument, NULL, 'd'},
        {"size", required_argument, NULL, 's'},
        {"position", required_argument, NULL, 'p'},
        {"columns", required_argument, NULL, 'c'},
        {"time", required_argument, NULL, 't'},
        {NULL, 0, NULL, 0}
    };
#endif
    time = 0;
    defaut = 0;
    position = NULL;
    size = NULL;

    options = cfg_new();

#ifdef HAVE_GETOPT_LONG
    while ((c = getopt_long(argc, argv, "hvdp:s:c:t:", long_options, NULL)) != -1)
#elif defined HAVE_GETOPT
    while ((c = getopt(argc, argv, "hvdp:s:c:t:")) != -1)
#endif
#if defined HAVE_GETOPT_LONG || defined HAVE_GETOPT
    {
        switch (c) {
        case 0:
            break;
        case 'h':   /* HELP */
            print_help(argv[0]);
        case 'v':   /* VERSION */
            print_version();
        case 'd':   /* DEFAULT */
            defaut = 1;
            break;
        case 'p':   /* POSITION */
            if (strutils_count_char(optarg, X_CHAR[0]) != 1) {
                print_help(argv[0]);
                break;
            }
            position = g_strsplit(optarg, X_CHAR, 2);
            if (position == NULL)
                print_help(argv[0]);
            else {
                if (position[0] == NULL ||
                    position[1] == NULL) {
                    g_strfreev(position);
                    print_help(argv[0]);
                }
            }
            break;
        case 's':   /* SIZE */
            if (strutils_count_char(optarg, X_CHAR[0]) != 1) {
                print_help(argv[0]);
                break;
            }
            size = g_strsplit(optarg, X_CHAR, 0);
            if (size == NULL)
                print_help(argv[0]);
            else {
                if (size[0] == NULL ||
                    size[1] == NULL) {
                    g_strfreev(size);
                    print_help(argv[0]);
                }
            }
            break;
        case 'c':
            if (strlen(optarg) != (CAPACITY_COLUMN + 1))
                columns = g_strdup(optarg);
            else
                print_help(argv[0]);
            break;
        case 't':
            time = atof(optarg);
            break;
        default:
            print_help(argv[0]);
            exit(0);
        }
    }

    /* Load default options ? */
    if (defaut)
        cfgfil_default(options);
    else
        cfgfil_read(options);

    /* New time ? */
    if (time != 0)
        options->update_interval = time;
    /* New window position ? */
    if (position != NULL) {
        options->window_position[0] = atoi(position[0]);
        options->window_position[1] = atoi(position[1]);
        g_strfreev(position);
    }
    /* New window size ? */
    if (size != NULL) {
        options->window_size[0] = atoi(size[0]);
        options->window_size[1] = atoi(size[1]);
        g_strfreev(size);
    }
    /* Which columns ? */
    if (columns != NULL) {
        for (i = 0 ; i <= CAPACITY_COLUMN ; i++)
            if (columns[i] == '1')
                options->show_columns[i] = 1;
        g_free(columns);
    }
#else
    cfgfil_read(options);
#endif

    return;
}

void
cfgfil_default (prefs_t *prefs)
{
    cfg_action = DEFAULT;
    cfgfil_rwd_values(prefs);

    return;
}

prefs_t *
cfg_new (void)
{
    prefs_t *prefs;

    prefs = g_malloc0(sizeof(prefs_t));

    return prefs;
}

void
cfg_free (prefs_t *prefs)
{
    gint i;

    for (i = 0 ; i < 2 ; i++) {
        g_free(prefs->mount_cmds[i]);
    }

    g_list_free_full(prefs->skip_fs, g_free);

    g_list_free_full(prefs->mountpoint_force, g_free);

    if (prefs != NULL)
        g_free(prefs);

    return;
}

void cfg_helper_glist_copy_clean(GList **dst, GList *src) {
    g_list_free_full(*dst, (GDestroyNotify)g_free);
    *dst = g_list_copy_deep(src, (GCopyFunc)g_strdup, NULL);
}

void
cfg_cpy (prefs_t *to, const prefs_t *from)
{
    gint i;

    to->update_interval = from->update_interval;
    to->show_columns_all = from->show_columns_all;
    for (i = 0 ; i < 2 ; i++) {
        to->window_position[i] = from->window_position[i];
        to->window_size[i] = from->window_size[i];
        to->sort[i] = from->sort[i];
        /* All */
        if (to->mount_cmds[i] != NULL)
            g_free(to->mount_cmds[i]);
        if (from->mount_cmds[i] != NULL)
            to->mount_cmds[i] = g_strdup(from->mount_cmds[i]);
        else
            to->mount_cmds[i] = g_strdup("");
    }
    for (i = 0 ; i < 3 ; i++) {
        to->color1[i] = from->color1[i];
        to->color2[i] = from->color2[i];
        to->color3[i] = from->color3[i];
        to->color_text[i] = from->color_text[i];
    }
    for (i = 0 ; i < 4 ; i++) {
        to->blocks[i] = from->blocks[i];
        to->gui_style[i] = from->gui_style[i];
        to->column_style[i] = from->column_style[i];
        to->capacity_style[i] = from->capacity_style[i];
        to->popup[i] = from->popup[i];
    }
    for (i = 0 ; i <= CAPACITY_COLUMN ; i++)
        to->show_columns[i] = from->show_columns[i];

    cfg_helper_glist_copy_clean(&to->skip_fs, from->skip_fs);
    cfg_helper_glist_copy_clean(&to->mountpoint_force, from->mountpoint_force);

    return;
}
